package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

public final class TaskServiceImpl extends AbstractUserOwnedService<Task, TaskRepository> implements TaskService {

    public TaskServiceImpl(@NotNull final TaskRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    @NotNull
    public List<Task> findByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

}
