package ru.t1.aayakovlev.tm.client;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

public abstract class AbstractEndpoint {

    @NotNull
    private final String host;

    @NotNull
    private final Integer port;

    @NotNull
    private Socket socket;

    public AbstractEndpoint(@NotNull String host, @NotNull Integer port) {
        this.host = host;
        this.port = port;
    }

    @NotNull
    protected Object call(@NotNull final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

}
