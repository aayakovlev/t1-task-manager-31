package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @NotNull
    public static final String NAME = "project-start-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
    }

}
