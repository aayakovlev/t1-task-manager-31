package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Task create.";

    @NotNull
    public static final String NAME = "task-create";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter description: ");
        @NotNull final String description = nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

}
