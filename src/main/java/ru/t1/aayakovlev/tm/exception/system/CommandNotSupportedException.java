package ru.t1.aayakovlev.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Passed command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Passed command '" + command + "' not supported...");
    }

}
