package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;

public interface SystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request);

}
