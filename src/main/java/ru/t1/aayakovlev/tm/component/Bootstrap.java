package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.endpoint.SystemEndpoint;
import ru.t1.aayakovlev.tm.endpoint.impl.SystemEndpointImpl;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;
import ru.t1.aayakovlev.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.*;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final CommandRepository commandRepository = new CommandRepositoryImpl();

    @Getter
    @NotNull
    private final CommandService commandService = new CommandServiceImpl(commandRepository);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final LoggerService loggerService = new LoggerServiceImpl();

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @Getter
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @Getter
    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final SystemEndpoint systemEndpoint = new SystemEndpointImpl(this);

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @Getter
    @NotNull
    private final UserService userService = new UserServiceImpl(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final AuthService authService = new AuthServiceImpl(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> commands = reflections.getSubTypesOf(AbstractCommand.class);
        commands.forEach(this::registry);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    private void initAdmin() throws AbstractException{
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initBackup() {
        backup.init();
    }

    private void initServer() {
        server.init();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    private boolean processArguments(@Nullable final String[] arguments) throws AbstractException {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;
        @Nullable final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(@Nullable final String argument) throws AbstractException {
        if (argument == null || argument.isEmpty()) return false;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        return true;
    }

    public void processCommand(@Nullable final String command) throws AbstractException {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void prepareStartup() throws AbstractException {
        initPID();
        initAdmin();
        loggerService.info(
                "___________              __                                                        \n" +
                "\\__    ___/____    _____|  | __   _____ _____    ____ _____     ____   ___________ \n" +
                "  |    |  \\__  \\  /  ___/  |/ /  /     \\\\__  \\  /    \\\\__  \\   / ___\\_/ __ \\_  __ \\\n" +
                "  |    |   / __ \\_\\___ \\|    <  |  Y Y  \\/ __ \\|   |  \\/ __ \\_/ /_/  >  ___/|  | \\/\n" +
                "  |____|  (____  /____  >__|_ \\ |__|_|  (____  /___|  (____  /\\___  / \\___  >__|   \n" +
                "               \\/     \\/     \\/       \\/     \\/     \\/     \\//_____/      \\/");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initBackup();
        initServer();
        initFileScanner();
    }

    public void prepareShutdown() {
        backup.stop();
        fileScanner.stop();
        server.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        try {
            @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
            registry(command);
        } catch (final @NotNull ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) throws AbstractException {
        if (processArguments(args)) return;
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command: ");
                @Nullable final String command = nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final AbstractException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
